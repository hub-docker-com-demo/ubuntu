# [Ubuntu security notices](https://usn.ubuntu.com/)

# Ubuntu specific Apt packages
* [ubuntu-minimal](https://packages.ubuntu.com/en/ubuntu-minimal)
* [ubuntu-standard](https://packages.ubuntu.com/en/ubuntu-standard)
* [ubuntu-meta](https://packages.ubuntu.com/en/source/focal/ubuntu-meta)

# init
* [*InitScriptHumanDescriptions*](https://wiki.ubuntu.com/InitScriptHumanDescriptions)

## [Systemd](https://freedesktop.org/wiki/Software/systemd/)
* [*SystemdForUpstartUsers*](https://wiki.ubuntu.com/SystemdForUpstartUsers)
* (fr) [*systemd*](https://doc.ubuntu-fr.org/systemd)

## [Upstart](http://upstart.ubuntu.com/)
* [*UpstartHowto*](https://help.ubuntu.com/community/UpstartHowto)
* (fr) [*Upstart*](https://doc.ubuntu-fr.org/upstart)

## Sys V
* (fr) [*Les scripts d'initialisation système V*](https://doc.ubuntu-fr.org/script_sysv)

## Speaking about
* [*ReplacementInit*](https://wiki.ubuntu.com/ReplacementInit) 2006-06-23
* [*ReplacementInitDiscussion*](https://wiki.ubuntu.com/ReplacementInitDiscussion) 2005-10-06

# LXD
* Recommanded installation is using [snapd](https://tracker.debian.org/pkg/snapd)
  * Indeed needs a working systemd
    * This apparently cannot be found today on [hub.docker.com](https://hub.docker.com/)
      * This is because systemd is incompatible with CI, because number one process cannot have a return value (in year 2021).
    * Maybe founding a way to run Ubuntu in Qemu could achive this on GitLab and alike.
      * The return value from process one issue will remain the same, probably.
* [*The LXD container hypervisor*](https://www.ubuntu.com/containers/lxd)
* [LXD](https://help.ubuntu.com/lts/serverguide/lxd.html.en)

# LXC
* lxc/[lxc](https://github.com/lxc/lxc)
* [LXC Downloads](https://linuxcontainers.org/lxc/downloads/)
* [*Everything You Need to Know about Linux Containers, Part II: Working with Linux Containers (LXC)*](https://www.linuxjournal.com/content/everything-you-need-know-about-linux-containers-part-ii-working-linux-containers-lxc) 2018 Petros Koutoupis
* [/etc/apparmor.d](https://google.com/search?q=%2Fetc%2Fapparmor.d)
* [lxc in docker](https://google.com/search?q=lxc+in+docker)
* [veth in docker](https://google.com/search?q=veth+in+docker)

## See also similar projects for LXC
* [alpinelinux-packages-demo/lxc](https://gitlab.com/alpinelinux-packages-demo/lxc)
